def call(String buildStatus = 'STARTED') {

    // Build status null equivale a SUCCESS

    buildStatus = buildStatus ?: 'SUCCESS'
    def color
    if (buildStatus == 'STARTED') {
        color = '#6666FF'
    } else if (buildStatus == 'SUCCESS') {
        color = '#00CD00'
    } else if (buildStatus == 'UNSTABLE') {
        color = '#FFFD3D'
    } else {
        color = '#FF0000'
    }
    def msg = "${buildStatus}: ${env.JOB_NAME} #${env.BUILD_NUMBER}:\n${env.BUILD_URL}"
    slackSend(color: color, message: msg)
}